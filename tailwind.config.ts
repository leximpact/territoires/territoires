import typography from "@tailwindcss/typography"
import daisyui from "daisyui"
import type { Config } from "tailwindcss"

const config: Config = {
  content: ["./src/**/*.{html,js,svelte,ts}"],

  plugins: [typography, daisyui],

  theme: {
    extend: {},
  },
}

export default config
