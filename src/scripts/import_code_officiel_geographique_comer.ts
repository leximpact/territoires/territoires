import fs from "fs-extra"
import Papa from "papaparse"

import type {
  CollectiviteOutreMer,
  TypeNomEnClair,
} from "$lib/code_officiel_geographique"

function parseCsvFile(filePath: string): Promise<unknown[]> {
  const readStream = fs.createReadStream(filePath)
  return new Promise((resolve) => {
    Papa.parse(readStream, {
      complete: ({ data }) => resolve(data),
      header: true,
      worker: true, // Don't bog down the main thread if its a big file.
    })
  })
}

const departements = (
  (await parseCsvFile("data/comer.csv")) as CollectiviteOutreMer[]
).map((collectivite) => {
  collectivite.TNCC = parseInt(
    collectivite.TNCC as unknown as string,
  ) as TypeNomEnClair
  return collectivite
})
await fs.writeJson("static/comer.json", departements, {
  spaces: 2,
})
