import type { Acteur } from "@tricoteuses/assemblee"
import fs from "fs-extra"
import path from "path"

import { mandatDeputeActifFromDepute } from "$lib/deputes"

const acteursDir =
  "data/AMO30_tous_acteurs_tous_mandats_tous_organes_historique_nettoye/acteurs"
const deputes: Acteur[] = []
for (const acteurFilename of await fs.readdir(acteursDir)) {
  const acteurFilePath = path.join(acteursDir, acteurFilename)
  if (acteurFilename.startsWith(".") || !acteurFilename.endsWith(".json")) {
    console.warn(`Skipping non-acteur file ${acteurFilePath}`)
    continue
  }
  const acteur = (await fs.readJson(acteurFilePath)) as Acteur
  // Keep only active députés.
  const mandatDeputeActif = mandatDeputeActifFromDepute(acteur)
  if (mandatDeputeActif?.election?.lieu.departement === undefined) {
    continue
  }
  deputes.push(acteur)
}

await fs.writeJson("static/deputes.json", deputes, {
  spaces: 2,
})
