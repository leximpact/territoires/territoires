import assert from "assert"
import fs from "fs-extra"
import XLSX from "xlsx"

import type { Epci } from "$lib/epci"

const communeKeys = ["CODGEO", "LIBGEO", "EPCI", "LIBEPCI", "DEP", "REG"]
const epciKeys = ["EPCI", "LIBEPCI", "NATURE_EPCI", "NB_COM"]
const sheetNames = [
  "EPCI",
  "Composition_communale",
  "Variables",
  "Documentation",
]

const workbook = XLSX.readFile("data/epci.xlsx")
assert.equal(workbook.SheetNames.length, sheetNames.length)
assert(
  workbook.SheetNames.every(
    (sheetName, index) => sheetName === sheetNames[index],
  ),
)

const epciSheet = workbook.Sheets.EPCI
let epciRows = XLSX.utils.sheet_to_json(epciSheet, { range: 5 }) as (Epci & {
  NB_COM?: number
})[]
const epciByCode: { [code: string]: Epci } = {}
for (const epci of epciRows) {
  const keys = Object.keys(epci)
  assert.equal(keys.length, epciKeys.length)
  assert(keys.every((key, index) => key === epciKeys[index]))
  epci.COM = []
  epciByCode[epci.EPCI] = epci
}

const commmunesSheet = workbook.Sheets.Composition_communale
const commmunesRows = XLSX.utils.sheet_to_json(commmunesSheet, {
  range: 5,
})
for (const commune of commmunesRows as Array<{ [key: string]: string }>) {
  const keys = Object.keys(commune)
  assert.equal(keys.length, communeKeys.length)
  assert(keys.every((key, index) => key === communeKeys[index]))
  const epci = epciByCode[commune.EPCI]
  epci.COM.push(commune.CODGEO)
}

for (const epci of epciRows) {
  assert.equal(epci.NB_COM, epci.COM.length)
  delete epci.NB_COM
}
// Last EPCI row is:
// {
// "EPCI": "ZZZZZZZZZ",
// "LIBEPCI": "Sans objet",
// "NATURE_EPCI": "ZZ",
// "COM": [
//     "22016",
//     "29083",
//     "29155",
//     "85113"
// ]
// }
// Delete it.
assert.equal(epciRows[epciRows.length - 1].NATURE_EPCI, "ZZ")
epciRows = epciRows.slice(0, -1)

await fs.writeJson("static/epci.json", epciRows, {
  spaces: 2,
})
