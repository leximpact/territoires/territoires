import fs from "fs-extra"
import Papa from "papaparse"

type Hexasmal = {
  code_commune_insee: string
  nom_de_la_commune: string
  code_postal: string
  libelle_d_acheminement: string
  ligne_5?: string
  _geopoint: string
}

function parseCsvFile(filePath: string): Promise<unknown[]> {
  const readStream = fs.createReadStream(filePath)
  return new Promise((resolve) => {
    Papa.parse(readStream, {
      complete: ({ data }) => resolve(data),
      header: true,
      worker: true, // Don't bog down the main thread if its a big file.
    })
  })
}

const distributionsPostales = (
  (await parseCsvFile("data/laposte_hexasmal.csv")) as Hexasmal[]
).map((ligne) => ({
  code_commune_insee: ligne.code_commune_insee,
  nom_de_la_commune: ligne.nom_de_la_commune,
  code_postal: ligne.code_postal,
  libelle_d_acheminement: ligne.libelle_d_acheminement,
  ligne_5: ligne.ligne_5 || undefined,
  coordonnees_geographiques: ligne._geopoint.split(",").map(parseFloat),
}))
await fs.writeJson(
  "static/distributions_postales.json",
  distributionsPostales,
  {
    spaces: 2,
  },
)
