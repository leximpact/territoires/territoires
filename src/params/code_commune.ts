import type { ParamMatcher } from "@sveltejs/kit"

export const match: ParamMatcher = (param) => /^\d[\dAB]\d{3}$/.test(param)
