export interface Access {
  access?: Access
  key: number | string
  parent?: unknown
}
