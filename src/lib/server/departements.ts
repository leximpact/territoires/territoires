import fs from "fs-extra"

import type {
  CollectiviteOutreMer,
  Departement,
} from "$lib/code_officiel_geographique"

const collectivitesOutreMer = (await fs.readJson(
  "static/comer.json",
)) as CollectiviteOutreMer[]
const departements = (await fs.readJson(
  "static/departement.json",
)) as Departement[]
export const departementByCode: {
  [code: string]: CollectiviteOutreMer | Departement
} = {
  ...Object.fromEntries(
    departements.map((departement: Departement) => [
      departement.DEP,
      departement,
    ]),
  ),
  ...Object.fromEntries(
    collectivitesOutreMer.map((collectivite: CollectiviteOutreMer) => [
      collectivite.COMER,
      collectivite,
    ]),
  ),
}
