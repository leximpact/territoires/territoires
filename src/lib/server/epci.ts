import fs from "fs-extra"

import type { Epci } from "$lib/epci"

const epciEntries = (await fs.readJson("static/epci.json")) as Epci[]
export const epciByCode: {
  [code: string]: Epci
} = Object.fromEntries(epciEntries.map((epci) => [epci.EPCI, epci]))
