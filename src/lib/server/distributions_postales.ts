import fs from "fs-extra"

import type { DistributionPostale } from "$lib/distributions_postales"

const distributionsPostales = (await fs.readJson(
  "static/distributions_postales.json",
)) as DistributionPostale[]
export const distributionsPostalesByCode: {
  [code: string]: DistributionPostale[]
} = {}
for (const codePostal of distributionsPostales) {
  let distributionsPostales =
    distributionsPostalesByCode[codePostal.code_commune_insee]
  if (distributionsPostales === undefined) {
    distributionsPostales = distributionsPostalesByCode[
      codePostal.code_commune_insee
    ] = []
  }
  distributionsPostales.push(codePostal)
}
