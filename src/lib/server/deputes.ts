import fs from "fs-extra"

import type { Acteur } from "@tricoteuses/assemblee"

import { mandatDeputeActifFromDepute } from "$lib/deputes"

export const deputeByCirconscriptionLegislativeCode = Object.fromEntries(
  ((await fs.readJson("static/deputes.json")) as Acteur[]).map((depute) => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const mandatDeputeActif = mandatDeputeActifFromDepute(depute)!
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const lieu = mandatDeputeActif.election!.lieu
    const departementCode =
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      lieu.numDepartement === "099" ? "99" : lieu.numDepartement!
    const circoCode =
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      departementCode + lieu.numCirco!.padStart(5 - departementCode.length, "0")
    return [circoCode, depute]
  }),
)
