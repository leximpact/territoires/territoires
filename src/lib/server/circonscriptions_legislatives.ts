import fs from "fs-extra"

import type { CirconscriptionLegislative } from "$lib/circonscriptions_legislatives"

const circonscriptionsLegislatives = (await fs.readJson(
  "static/circonscriptions_legislatives.json",
)) as CirconscriptionLegislative[]
export const circonscriptionLegislativeByCode: {
  [code: string]: CirconscriptionLegislative
} = Object.fromEntries(
  circonscriptionsLegislatives.map((circo) => [circo.code, circo]),
)

export function departementCodeFromCirconscriptionLegislativeCode(
  code: string,
) {
  const departementCode = code.slice(0, 2)
  return ["97", "98"].includes(departementCode)
    ? code.slice(0, 3)
    : departementCode
}
