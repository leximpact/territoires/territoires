import fs from "fs-extra"

import type {
  Commune,
  CommuneCollectiviteOutreMer,
} from "$lib/code_officiel_geographique"

const communes = (await fs.readJson("static/commune.json")) as Commune[]
const communesCollectiviteOutreMer = (await fs.readJson(
  "static/com_comer.json",
)) as CommuneCollectiviteOutreMer[]
export const communeByCode: {
  [code: string]: Commune | CommuneCollectiviteOutreMer
} = {
  ...Object.fromEntries(
    communes.map((commune: Commune) => [commune.COM, commune]),
  ),
  ...Object.fromEntries(
    communesCollectiviteOutreMer.map((commune: CommuneCollectiviteOutreMer) => [
      commune.COM_COMER,
      commune,
    ]),
  ),
}
