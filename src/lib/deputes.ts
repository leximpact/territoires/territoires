import {
  type Acteur,
  CodeTypeOrgane,
  type Mandat,
  TypeMandat,
} from "@tricoteuses/assemblee"

const now = new Date().toISOString()

export function mandatDeputeActifFromDepute(
  depute: Acteur,
): Mandat | undefined {
  return depute.mandats?.filter(
    (mandat) =>
      mandat.xsiType === TypeMandat.MandatParlementaireType &&
      mandat.typeOrgane === CodeTypeOrgane.Assemblee &&
      mandat.dateDebut.toString() <= now &&
      (!mandat.dateFin || mandat.dateFin.toString() >= now),
  )[0]
}
