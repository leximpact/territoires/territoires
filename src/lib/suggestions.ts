import type { Acteur } from "@tricoteuses/assemblee"

import type {
  CirconscriptionLegislativeAutocompletion,
  CommuneAutocompletion,
  DepartementAutocompletion,
  EpciAutocompletion,
} from "$lib/autocompletions"
import type { CirconscriptionLegislative } from "$lib/circonscriptions_legislatives"
import type {
  Commune,
  CommuneCollectiviteOutreMer,
  CollectiviteOutreMer,
  Departement,
} from "$lib/code_officiel_geographique"
import type { DistributionPostale } from "$lib/distributions_postales"
import type { Epci } from "$lib/epci"

export interface CirconscriptionLegislativeSuggestion extends SuggestionBase {
  circonscription_legislative?: CirconscriptionLegislative
  communes?: Array<Commune | CommuneCollectiviteOutreMer>
  depute?: Acteur
  role?: CirconscriptionLegislativeAutocompletion["role"]
}

export interface CommuneSuggestion extends SuggestionBase {
  commune?: Commune | CommuneCollectiviteOutreMer
  distributions_postales?: Array<DistributionPostale>
  role?: CommuneAutocompletion["role"]
}

export interface DepartementSuggestion extends SuggestionBase {
  departement?: CollectiviteOutreMer | Departement
  communes?: Array<Commune | CommuneCollectiviteOutreMer>
  role?: DepartementAutocompletion["role"]
}

export interface EpciSuggestion extends SuggestionBase {
  epci?: Epci
  communes?: Array<Commune | CommuneCollectiviteOutreMer>
  role?: EpciAutocompletion["role"]
}

export interface SuggestionBase {
  autocompletion: string
  code: string
  libelle: string
  // match: …
  score: number
  // terms: string[]
}
