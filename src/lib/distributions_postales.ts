export interface DistributionPostale {
  code_commune_insee: string
  nom_de_la_commune: string
  code_postal: string
  libelle_d_acheminement: string
  /**
   * Ligne 5 dans l'écriture de l'adresse, utilisée notamment pour préciser l'ancienne commune ou le lieu-dit
   */
  ligne_5?: string
  coordonnees_geographiques: [number, number] // latitude, longitude
}
