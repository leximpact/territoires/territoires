export interface CommuneDgcl {
  "Code INSEE de la commune": string
  "Nom de la commune": string
  "Code département de la commune": string
  "Code SIREN de l'EPCI"?: string
  "Nom de l'EPCI"?: string
}

export interface DepartementDgcl {
  "Numéro département": string
  "Nom département": string
  "Numéro région": string
}
